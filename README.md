poetry run python main.py


docker build --no-cache -t producer -f Dockerfile .

docker run -d --name producer  --restart always -p 80:55000/tcp  producer:latest

pip3 freeze > requirements.txt 
