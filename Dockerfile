FROM python:3.10
WORKDIR /code

COPY ./Producer/requirements.txt /code/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY ./Producer .


CMD ["python", "main.py"]

