import random
import time
import asyncio
import aioredis


async def report_measurement(redis):
    """
    It reports a measurement every second, with a random device ID and a random measurement value
    :param redis: The Redis client instance
    """
    min_measurement = 10
    max_measurement = 100

    stream_name = "measurements"

    while True:
        device_id = f"abc{random.randint(1, 5)}"
        measurement = f"{random.randint(min_measurement, max_measurement)} kwh"

        timestamp = int(time.time())

        await redis.xadd(stream_name, {
            "device_id": device_id,
            "measurement": measurement,
            "timestamp": timestamp
        })
        print(f"ID Dispositivo: {device_id}")
        print(f"Métrica 1 medida: {measurement}")
        print(f"Timestamp: {timestamp}")

        await asyncio.sleep(1)


async def main():
    """
    It connects to the Redis server, and then calls the `report_measurement` function
    """
    redis = await aioredis.from_url("redis://redis:6379")
    await report_measurement(redis)

if __name__ == "__main__":
    asyncio.run(main())
